﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSceneTrigger : MonoBehaviour
{

	public int leveltoload;


	void OnTriggerEnter(Collider collision)
	{
		if (collision.CompareTag ("Player"))
		{
            var Fade = FindObjectOfType<CameraFade>();
                
            StartCoroutine(Fade.FadeOut());
			Application.LoadLevel (leveltoload);
            
		}
	}
}