﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadingNotes2 : MonoBehaviour
{

    public GameObject pic;
    public AudioClip sound;
    public GameObject text;
    public GameObject Briefinhalt;


    void start()
    {
        pic.SetActive(false);
        text.SetActive(false);
        Briefinhalt.SetActive(false);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            pic.SetActive(false);
            text.SetActive(false);
            Briefinhalt.SetActive(false);
        }

    }
    private void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "attackArea")
        {
            if(Input.GetKeyDown(KeyCode.Mouse0))
            {
                text.SetActive(true);
                pic.SetActive(true);
                Briefinhalt.SetActive(true);

                AudioSource.PlayClipAtPoint(sound, transform.position);
            }
            return;
        }
    }
}
