﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxtrigger : MonoBehaviour
{

    public GameObject uiObject;
    public int SecondsDisplayed;
    
    
    void Start()
    {
        uiObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider player)
    {
       if(player.gameObject.tag == "Player")
        {
            uiObject.SetActive(true);
            StartCoroutine("WaitForSec");
        }
    }
    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(SecondsDisplayed);
        Destroy(uiObject);
        Destroy(gameObject);
    }



}



    




