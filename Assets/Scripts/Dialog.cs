﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialog : MonoBehaviour
{
    public TextMeshProUGUI    textDisplay;
    public string[] sentences;
    public int index;
    public float typingSpeed;
    public GameObject playerhere;
   


   
  
    public void Start()
    {

        StartCoroutine(Type());

    }
    // typing effect

    public IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }




    public void OnTriggerEnter (Collider other)
    {
        if (index < sentences.Length - 1)
           if (other.gameObject.tag == "Player")
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
    }


}
