﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutandLoad : MonoBehaviour {

	// Use this for initialization
	IEnumerator Start()
    {
        yield return new WaitForSecondsRealtime(2);
        var Fadeout = FindObjectOfType<CameraFade>();
        yield return Fadeout.FadeOut();
        Destroy(this.gameObject);
        

    

    }
}
