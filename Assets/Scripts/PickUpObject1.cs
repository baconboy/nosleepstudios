
using UnityEngine;
using TMPro;

public class PickUpObject1 : MonoBehaviour
{

	public bool adjustObject = false;
	public bool rotateHorizontal = false;
	public bool rotateVertical = false;
    public TextMeshPro label;



    public void DisplayInteraction()

    {
        label.color = Color.black;

    }
    public void StopDisplaying()
    {
        label.color = Color.clear;
    }

}
