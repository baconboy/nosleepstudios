﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CameraFade : MonoBehaviour
{



    [SerializeField, Tooltip("Link the image to fade here!")]
    private Image screen;

    [SerializeField]
    private Color fadeStart;

    [SerializeField]
    private Color fadeEnd;

    [SerializeField]
    private float targetTime = 1f;

    [SerializeField]
    private AnimationCurve ease;

    public IEnumerator FadeIn()
    {
        yield return FadeColor(fadeStart, fadeEnd);
    }
    private void Start()
    {
        StartCoroutine(FadeOut());
    }

    public IEnumerator FadeColor(Color start, Color end)
    {
        var deltaTime = Time.deltaTime;
        var progress = 0f;

        while (targetTime >= deltaTime)
        {
            // Calculates the procentual progress of time
            progress = deltaTime * (1f / targetTime);

            // Lerps the color over time
            screen.color = Color.Lerp(start, end, ease.Evaluate(progress));

            // Tracks the time in seconds
            deltaTime += Time.deltaTime;

            // Wait one frame
            yield return null;
        }

        // Set End Result
        screen.color = end;

    }
    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public IEnumerator FadeOut()
    {
        yield return FadeColor(fadeEnd, fadeStart);
    }
}
