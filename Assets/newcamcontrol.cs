﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class newcamcontrol : MonoBehaviour
{

    public float speed;
    public float sensitivity;
    Rigidbody rb;

    public GameObject eyes;

    float moveFB;
    float moveLR;

    float rotX;
    float rotY;

    float maxClamp = 90;
    float minClamp = -90;

    bool grounded;

    void Start()
    {
        Cursor.visible = false;
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        moveFB = Input.GetAxis("Vertical") * speed;
        moveLR = Input.GetAxis("Horizontal") * speed;

        rotX = Input.GetAxis("Mouse X") * sensitivity;
        rotY = Input.GetAxis("Mouse Y") * sensitivity;

        Vector3 movement = new Vector3(moveLR, 0, moveFB);
        transform.Rotate(0, rotX, 0);
        eyes.transform.Rotate(-rotY, 0, 0);

        movement = transform.rotation * movement;
        rb.MovePosition(transform.position + (movement * Time.deltaTime));

        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb.AddForce(new Vector3(0, 100, 0), ForceMode.Impulse);
        }

        //Add x axis clamp here
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Entered");
        if (collision.gameObject.CompareTag("Ground"))
        {
            grounded = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        Debug.Log("Exited");
        if (collision.gameObject.CompareTag("Ground"))
        {
            grounded = false;
        }
    }
}

