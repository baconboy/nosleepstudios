﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogportal : MonoBehaviour {

    [SerializeField]
    public GameObject Aftertriggerobject;

    private void OnTriggerEnter(Collider other)
    {
        Aftertriggerobject.SetActive(true);
    }
}
